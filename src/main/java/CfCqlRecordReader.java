import com.datastax.driver.core.*;
import org.apache.cassandra.hadoop.cql3.CqlConfigHelper;
import org.apache.hadoop.mapreduce.RecordReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.mapreduce.InputSplit;
import org.apache.hadoop.mapreduce.TaskAttemptContext;
import org.apache.cassandra.hadoop.ConfigHelper;
import org.apache.cassandra.hadoop.HadoopCompat;
import org.apache.cassandra.hadoop.cql3.CqlRecordReader;
import org.apache.cassandra.utils.Pair;

public class CfCqlRecordReader extends RecordReader<Long, CfRow>
    implements org.apache.hadoop.mapred.RecordReader<Long, CfRow> {
    private static final Logger logger = LoggerFactory.getLogger(CfCqlRecordReader.class);

    private String[] columnFamilies;
    private ArrayList<CqlRecordReader> cfRecordReaders = new ArrayList<CqlRecordReader>();

    private Pair<Long, CfRow> currentRow;
    private Token currentRowToken;
    private Long totalRead;

    public CfCqlRecordReader()
    {
        super();
    }

    @Override
    public boolean nextKeyValue() throws IOException, InterruptedException
    {
      int currentLineTotal = 0;
      Boolean isParellel = false;

      while (!isParellel)
      {
          for (CqlRecordReader cfRecordReader : cfRecordReaders)
          {
              if(isParellel) break;
              while(true)
              {
                  if (!cfRecordReader.nextKeyValue())
                  {
                        logger.info("Finished scanning {} rows", totalRead);
                        return false;
                  }
                  Token token = cfRecordReader.getCurrentValue().getPartitionKeyToken();
                  if(currentRowToken != null && token.compareTo(currentRowToken) < 0)
                      continue;
                  if (currentRowToken == null || token.compareTo(currentRowToken) > 0)
                  {
                      currentRowToken = token;
                      currentLineTotal = 1;
                  }
                  else if(token.compareTo(currentRowToken) == 0)
                  {
                      currentLineTotal++;
                  }
                  if(currentLineTotal == columnFamilies.length)
                  {
                      isParellel = true;
                  }
                  break;
                }
          }
      }
      CfRowInner mergeRow = new CfRowInner();
      for (int i = 0; i < columnFamilies.length; ++i)
      {
    	  mergeRow.add(columnFamilies[i], cfRecordReaders.get(i).getCurrentValue());
      }
      currentRow = Pair.create(totalRead++, mergeRow);
      return true;
    }

    @Override
    public Long getCurrentKey() throws IOException, InterruptedException
    {
        return currentRow.left;
    }

    @Override
    public CfRow getCurrentValue() throws IOException, InterruptedException
    {
        return currentRow.right;
    }

    @Override
    public boolean next(Long key, CfRow value) throws IOException
    {
      try
      {
    	  if (nextKeyValue())
		  {
    		  value = getCurrentValue();
    		  return true;
    	  }
      } catch (InterruptedException e) {
		  logger.trace(e.toString());
	  }
      return false;
    }

    @Override
    public Long createKey()
    {
        return Long.valueOf(0L);
    }

    @Override
    public CfRow createValue()
    {
        return new CfRowInner();
    }

    @Override
    public long getPos() throws IOException
    {
        return totalRead;
    }

    @Override
    public void close() throws IOException
    {
        for (CqlRecordReader cfRecordReader : cfRecordReaders)
        {
            cfRecordReader.close();
        }
    }

    @Override
    public float getProgress() throws IOException
    {
    	float progess = 0.0F;
    	for(CqlRecordReader cfRecordReader : cfRecordReaders)
    	{
    		progess = Math.max(progess, cfRecordReader.getProgress());
    	}
    	return progess;
    }

	public void initialize(InputSplit s, TaskAttemptContext tac) throws IOException, InterruptedException
    {
        Configuration conf        = HadoopCompat.getConfiguration(tac);
        String        keyspace    = ConfigHelper.getInputKeyspace(conf);
        String        oldCfConfig = ConfigHelper.getInputColumnFamily(conf);

        columnFamilies = oldCfConfig.split(":");
        totalRead = 0L;
        currentRowToken = null;

        // Prepare metadata to retrieve all columns.
        Cluster cluster = CqlConfigHelper.getInputCluster(s.getLocations(), conf);
        Session session = cluster.connect(keyspace);

        for (int i = 0; i < columnFamilies.length; ++i)
        {
            // CqlRecordReader set column family by job Configuration

            // Build cql query
            String cf = columnFamilies[i].toLowerCase();


            ConfigHelper.setInputColumnFamily(conf, keyspace, cf);
            TableMetadata tableMetadata = session.getCluster()
                                                 .getMetadata()
                                                 .getKeyspace(Metadata.quote(keyspace))
                                                 .getTable(Metadata.quote(cf));
            String[] colNames = new String[tableMetadata.getColumns().size()];
            for (int j = 0; j < colNames.length; j++) {
                colNames[j] = tableMetadata.getColumns().get(j).getName();
            }

            ArrayList<String> partitionKeys = new ArrayList<>();
            for (ColumnMetadata partitionKey : tableMetadata.getPartitionKey()) {
                partitionKeys.add(quote(partitionKey.getName()));
            }
            String partitionKey = String.join(", ", partitionKeys);
            String selectColumnList = "token(" + partitionKey + "), " + String.join(", ", colNames);
            String cql = String.format("SELECT %s FROM %s.%s WHERE token(%s)>? AND token(%s)<=?",
                    selectColumnList, quote(keyspace), quote(cf), partitionKey, partitionKey);
            logger.info("record reader cql: " + cql);

            CqlConfigHelper.setInputCql(conf, cql);
            CqlRecordReader cfRecordReader = new CqlRecordReader();
            cfRecordReader.initialize(s, tac);
            cfRecordReaders.add(cfRecordReader);
        }
        ConfigHelper.setInputColumnFamily(conf, keyspace, oldCfConfig);
    }

    private String quote(String identifier)
    {
        return "\"" + identifier.replaceAll("\"", "\"\"") + "\"";
    }
}
