/**
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import java.io.File;
import java.io.IOException;

import org.apache.cassandra.hadoop.cql3.CqlConfigHelper;
import org.apache.commons.io.FileUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.apache.cassandra.hadoop.ConfigHelper;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.Mapper;
import org.apache.hadoop.mapreduce.Reducer;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;

/**
 * This counts the occurrences of words in ColumnFamily
 *   cql3_wordcount ( id uuid,
 *                   line  text,
 *                   PRIMARY KEY (id))
 *
 * For each word, we output the total number of occurrences across all body texts.
 *
 * When outputting to Cassandra, we write the word counts to column family
 *  output_words ( word text,
 *                 count_num text,
 *                 PRIMARY KEY (word))
 * as a {word, count} to columns: word, count_num with a row key of "word sum"
 */
public class CfApp
{
    private static final Logger logger = LoggerFactory.getLogger(CfApp.class);

    static final String KEYSPACE = "bigdata_mapred_10";

    static final String COLUMN_FAMILY = "src0:src1:src2:src3:src4:src5:src6:src7:src8:src9";

    private static final String OUTPUT_PATH_PREFIX = "/tmp/bigdata_mapred";

    public static void main(String[] args) throws Exception
    {
        FileUtils.deleteDirectory(new File(OUTPUT_PATH_PREFIX));

        Configuration conf = new Configuration();
        Job job = Job.getInstance(conf, "bigdata_mapred");
        job.setJarByClass(CfApp.class);

        job.setCombinerClass(ReducerToFilesystem.class);
        job.setReducerClass(ReducerToFilesystem.class);
        job.setOutputKeyClass(Text.class);
        job.setOutputValueClass(IntWritable.class);
        FileOutputFormat.setOutputPath(job, new Path(OUTPUT_PATH_PREFIX));

        job.setMapperClass(NativeTokenizerMapper.class);
        job.setInputFormatClass(CfCqlInputFormat.class);

        ConfigHelper.setInputColumnFamily(job.getConfiguration(), KEYSPACE, COLUMN_FAMILY);
        ConfigHelper.setInputInitialAddress(job.getConfiguration(), "localhost");
        ConfigHelper.setInputPartitioner(job.getConfiguration(), "Murmur3Partitioner");
        CqlConfigHelper.setInputCQLPageRowSize(job.getConfiguration(), "3");;

        // TODO Record the execution time and log it.
        // Run jobs
        long startTime=System.currentTimeMillis();   //获取开始时间  
        job.waitForCompletion(true);
        long endTime=System.currentTimeMillis(); //获取结束时间  
        System.out.println("Running Time"+(endTime-startTime)+"ms"); 
    }

    public static class NativeTokenizerMapper extends Mapper<Long, CfRow, Text, IntWritable>
    {
        private final static IntWritable one = new IntWritable(1);
        private Text word = new Text();

        protected void setup(Context context)
        {
            logger.info("tokenizer setup");
        }

        public void map(Long key, CfRow row, Context context) throws IOException, InterruptedException
        {
            String value = row.getString("src0", "src0");
            //logger.info("src0: " + value);
            word.set(value);
            context.write(word, one);
        }
    }

    public static class ReducerToFilesystem extends Reducer<Text, IntWritable, Text, IntWritable>
    {
        public void reduce(Text key, Iterable<IntWritable> values, Context context) throws IOException, InterruptedException
        {
            int sum = 0;
            for (IntWritable val : values)
                sum += val.get();
            context.write(key, new IntWritable(sum));
        }
    }
}
